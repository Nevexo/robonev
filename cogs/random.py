import datetime, discord, urllib, wikipedia, time, re, random, json, config
from helpers import track

from cowpy import cow

from discord.ext import commands

import requests

class Random:
    def __init__(self, bot):
        self.bot = bot

    def get_mac_address(self, mac):
        r = requests.get(f'https://api.macvendors.com/{mac}')
        if r.status_code == 200:
            return r.text
        else:
            return False

    @commands.command(aliases=["latestxkcd"])
    async def xkcd(self, ctx, post=""): # Latest post = ID 0
        """ Shows a specific xkcd comic, if no comic number is provided the latest will be displayed."""
        track.exec_cmd("xkcd Post (r.xkcd)")
        msg = await ctx.send(":clock10: Parsing xkcd post...")

        try: 
            xkcdJson = await self.bot.aiojson(f"https://xkcd.com/{post}/info.0.json") # Get xkcd post (or use 0 for latest)
        except:
            msg.edit(content=":x: Failed to get that comic, does it exist?")

        imageUrl = "img" in xkcdJson # Get img URL from xkcd API respose
        
        if not imageUrl:
            msg.edit(content=":x: Something is wrong with xkcd...")
            return # If the post doesn't exist, fail the command.

        xkcd_timestamp = datetime.datetime.strptime(f"{xkcdJson['year']}-{xkcdJson['month']}-{xkcdJson['day']}",
                                                    "%Y-%m-%d") # Credit: gitlab.com/ao (https://gitlab.com/ao/AveBot/blob/rewrite-two/cogs/fun.py#L91)

        # Create xkcd embed, designed with https://leovoel.github.io/embed-visualizer/
        title = f"{xkcdJson['safe_title']} (#{xkcdJson['num']})"
        embed = discord.Embed(title=title, colour=discord.Colour(0xc486e), timestamp=xkcd_timestamp)

        embed.set_image(url=xkcdJson["img"])
        embed.set_author(name="xkcd comics", url="https://xkcd.com")
        embed.set_footer(text=xkcdJson["alt"])

        await msg.edit(content="", embed=embed)

    @commands.command(aliases=["letmegooglethatforyou", "googleit", "google"])
    async def lmgtfy(self, ctx, *, query="How to use a command on a discord bot"):
        """
            Let me google that for you - run with a query"""
        track.exec_cmd("Let me Google that for you (r.lmgtfy)")
        await ctx.send(f"http://lmgtfy.com/?q={urllib.parse.quote(query)}")

    @commands.command(aliases=["wikipedia", "tellmeabout"])
    async def wiki(self, ctx, *, query:str):
        """ Get information on a subject frmo wikipedia"""
        track.exec_cmd("Wikipedia loopup (r.wiki)")
        msg = await ctx.send(":clock10: Downloading article...")
        query = re.escape(query)
        try:
            page = wikipedia.page(query)
            await msg.edit(content=":clock11: Parsing article...")
            # Create embed. (Made with https://leovoel.github.io/embed-visualizer/)
            summary = page.summary
            summary = (summary[:1000] + "...") if len(summary) > 1000 else summary
            embed = discord.Embed(title=f"wikipedia.org/wiki/{query}",
             colour=discord.Colour(0xbbf2c2),
             url=f"https://wikipedia.org/wiki/{query}",
             description=f"```{summary}``` [read more](https://wikipedia.org/wiki/{query})")

            embed.set_thumbnail(url=page.images[1])
            embed.set_author(name="Wikipedia", url="https://wikipedia.org",
            icon_url="https://upload.wikimedia.org/wikipedia/commons/thumb/d/de"
            "/Wikipedia_Logo_1.0.png/220px-Wikipedia_Logo_1.0.png")

            embed.add_field(name="Revision Number", value=page.revision_id)
            await msg.edit(content="", embed=embed)
        except:
            await msg.edit(":x: Failed to get wikipedia page.")

    @commands.command(aliases=["macaddr", "macaddress"])
    async def mac(self, ctx, macaddr:str):
        """ Get the vendor of a MAC address (from MacVendors API) """
        track.exec_cmd("MAC Address Vendor Information (r.mac)")
        msg = await ctx.send(f":clock10: Looking up MAC `{macaddr}`")
        vendor = self.get_mac_address(urllib.parse.quote(macaddr))
        if vendor == False:
            await msg.edit(content="Failed to find MAC Address vendor.")
        else:
            await msg.edit(content=f":white_check_mark: Vendor name: `{vendor}`"
            " - Powered by <https://macvendors.com>")

    @commands.command()
    async def snowflake(self, ctx, snowflake: int):
        """
        Convert a Discord (or Twitter) snowflake into a timestamp
        and collect any other data from it."""
        track.exec_cmd("Snowflake Calculator (r.snowflake)")

        if len(str(snowflake)) != 18:
            ctx.send(":x: Invalid snowflake length. It should be 18 <https://discordapp.com/developers/docs/reference#snowflakes>")
        else:
            stamp = int((snowflake / 4194304) + 1420070400000) # Convert denary snowflake into the timestamp (first 42 bits)

            utcStamp = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(stamp/1000)) # Stamp is a 42 bit **ms** epoch
            await ctx.send(f"Snowflake Timestamp: {utcStamp} (UTC)")

    @commands.command()
    async def cowsay(self, ctx, *, message: str):
        """
        Have a cow say your message!
        Randomly picks a style of cow.
        """
        track.exec_cmd("Cowsay (r.cowsay)")
        if len(message) > 150:
            await ctx.send(":x: Oof, that's a long message. We don't wanna get ***TRUNCATED*** now do we?")
        else:
            type = random.randint(0, 2) # Pick cow style
            # Never trust the user
            #message = re.escape(message)

            if type == 0:
                cow_message = cow.Moose(thoughts=True).milk(message)
            elif type == 1:
                cow_message = cow.Moose(tongue=True).milk(message)
            elif type == 2:
                cow_message = cow.milk_random_cow(message) # Even more randomness!                
            
            await ctx.send(f"```{cow_message}```")
    
    @commands.command()
    async def release(self, ctx):
        """
        See release notes for this version of R. Nev"""
        track.exec_cmd("Release information (r.release)")
        with open('extra_files/release.json') as f:
            data = json.load(f) # Load release.json file.
            invalid = False # Set to true if something is wrong.
            if data["require_target"]:
                if data["target_version"] != config.version:
                    invalid = True # Version missmatch

            if not invalid: # Version check disabled or pass OK.
                embed = discord.Embed(title=data["title"], description=data["message"])

                embed.set_author(name="R. Nev Release Notices",
                 icon_url='https://cdn.discordapp.com/avatars/'
                 '169471176643575809/57bb72c51cbfa542a4e7e75944395cc6.webp?size=1024')

                for section in data["sections"]:
                    embed.add_field(name=section["title"], value=section["message"])

                await ctx.send(content="", embed=embed)

            else:
                ctx.send(content="Aw shucks. There's no change log for this version.")

    @commands.command()
    async def stats(self, ctx):
        """
        See stats about R. Nev"""
        track.exec_cmd("Stats page (r.stats)")
        most_used_cmd = track.most_used_cmd() 
        members         = 0
        guild_count     = len(self.bot.guilds)
        messages_proc   = track.fetch_messages()
        oh_counter      = track.fetch_ohs()

        for guild in self.bot.guilds:
            members += len(guild.members)

        embed = discord.Embed(title="R. Nev Statistics",
         description="Information that R. Nev collects daily.\n\n_Information valid to the last R. Nev restart/update_")

        embed.add_field(name="Most Used Command", value=f"```{most_used_cmd}```", inline=False)
        embed.add_field(name="Messages Processed", value=f"```{messages_proc}```", inline=True)
        embed.add_field(name="Connected Guilds", value=f"```{guild_count}```", inline=True)
        embed.add_field(name="Members", value=f"```{members}```", inline=True)
        embed.add_field(name="'oh' messages", value=f"```{oh_counter}```", inline=True)

        await ctx.send(content="", embed=embed)

    @commands.command()
    async def kernel(self, ctx, moniker=""):
        """
        Get the latest kernel.org information"""
        msg = await ctx.send(":clock10: Getting information...")

        try: 
            kernelJson = await self.bot.aiojson(f"https://kernel.org/releases.json") # Get kernel.org dfata
        except:
            msg.edit(content=":x: Failed to get data. Tux is on a break.")

        stable_version = kernelJson["latest_stable"]["version"]
        releases = kernelJson["releases"]

        # Generate embed

        if moniker == "":
            # No moniker supplied, list the available ones:
            embed = discord.Embed(title="Available Kernel Monikers", colour=discord.Colour(0xf5bd0c),
                url="https://kernel.org",
                description=f"The latest (stable) Kernel Version is **{stable_version}**."
                "\nUse `r.kernel <moniker>` to view specific (i.e `r.kernel mainline`)."
                "\nBelow are the available monikers:")
            
            embed.set_footer(text="Data provided by Kernel.org")
            for branch in releases:
                information = f"Version: `{branch['version']}`"
                if branch["iseol"]:
                    branch["moniker"] += " (EOL)"
                embed.add_field(name=branch["moniker"],
                value=information)

            await msg.edit(content="", embed=embed)

        else:
            # Find the moniker
            found = False
            counter = 0
            arrayLocation = False
            for branch in releases:
                if branch['moniker'] == moniker:
                    found = True
                    arrayLocation = counter

                counter += 1
            
            # Moniker supplied, check if it exists:
            if found != False:
                # Valid, display it:
                moniker = releases[arrayLocation]
                embed = discord.Embed(title="Kernel Moniker Information",

                colour=discord.Colour(0xf5bd0c),
                url="https://kernel.org",
                description=f"The latest ({moniker['moniker']}) Kernel Version is **{moniker['version']}**.")

                # Build the fields:
                if moniker["iseol"]:
                    embed.add_field(name="⚠ End of life",
                    value="This branch as reached it's designated end of life.")

                if moniker["source"] != None:
                    embed.add_field(name="🔁 Download source",
                    value=f"[Download (tar.xz)]({moniker['source']})")
                
                if moniker["changelog"] != None:
                    embed.add_field(name="ℹ Changelog",
                    value=f"A changelog is available, view it [here]({moniker['changelog']})")

                if moniker["diffview"] != None:
                    embed.add_field(name=":white_check_mark: Diff",
                    value=f"View the [git diff]({moniker['diffview']})")
                
                embed.add_field(name="📅 Release Date",
                value=f"{moniker['moniker']}/{moniker['version']} was released on {moniker['released']['isodate']}")

                await msg.edit(content="", embed=embed)

            else:
                await msg.edit(content=":x: Invalid moniker, use `r.kernel` to see available monikers.")

    @commands.command()
    async def cat(self, ctx, status_code:str):
        """
        Get a HTTP cat deliveried right to your channel!"""
        
        title = f"HTTP {status_code} - Catified"
        embed = discord.Embed(title=title, colour=discord.Colour(0xc486e))

        embed.set_image(url=f"https://http.cat/{status_code}.jpg")
        embed.set_footer(text="These images are provided by http.cat")

        await ctx.send(content="", embed=embed)

def setup(bot):
    bot.add_cog(Random(bot))