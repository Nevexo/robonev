# Modules to do with internet security & safety

from discord.ext import commands
from helpers import track

import requests, urllib, datetime, discord

hibp = {"base": "https://haveibeenpwned.com",
        "api_endpoint": "https://haveibeenpwned.com/api/v2/breachedaccount/"}

class Security:

    def __init__(self, bot):
        self.bot = bot

    def get_hibp_stats(self, email):
        r = requests.get(f"{hibp['api_endpoint']}{urllib.parse.quote(email)}"
        , headers={"User-Agent": "RoboNev HIBP checker"})
        if r.status_code == 404:
            return "NOT_BREACHED"
        elif r.status_code == 200:
            return r.json()
        else:
            return False

    @commands.command(aliases=["haveibeenpwned"])
    async def hibp(self, ctx, *, email:str):
        """
        Check if you've been pwned! """
        track.exec_cmd("Have I been Pwned? (r.hibp)")
        msg = await ctx.send(":clock10: Contacting HaveIBeenPwned...")
        pwned = self.get_hibp_stats(email)
        breachLimit = 5
        if pwned == False:
            # Failed to get breaches.
            await msg.edit(content=f":x: Failed to get pwned data,"
                     f" you can check manually at {hibp['base']}")
        elif pwned == "NOT_BREACHED":
            # No breaches found!
            await msg.edit(content=":white_check_mark: Congratulations,"
            " your account hasn't been breached!")
        else:
            # Oh no! Pwned!
            # Create the embed (by https://leovoel.github.io/embed-visualizer/)
            embed = discord.Embed(title="Oh no - Pwned!",
             colour=discord.Colour(0x771623),
             url=f"{hibp['base']}", 
             description=f"Your account has been found in `{len(pwned)}` breaches!",
             timestamp=datetime.datetime.now())

            embed.set_thumbnail(url="https://haveibeenpwned.com/Content/Images/SocialLogo.png")
            embed.set_author(name="Have I Been Pwned?", url="https://haveibeenpwned.com")
            embed.set_footer(text="HIBP is a service operated by Troy Hunt, see more in r.credits",
             icon_url="https://cdn.discordapp.com/embed/avatars/0.png")

            addedBreaches = 0

            for breach in pwned:
                breachedData = ""
                for dataType in breach['DataClasses']:
                    breachedData = breachedData + dataType + "\n"

                description = f"Breached: `{breach['BreachDate']}`\nPwned: `{breach['PwnCount']}`\nBreached Data: ```{breachedData}```"
                # That's a really long line.

                if addedBreaches != breachLimit:
                    addedBreaches += 1
                    embed.add_field(name=breach['Name'], value=description)

            if addedBreaches == breachLimit:
                extraBreaches = len(pwned)-breachLimit

                embed.add_field(name=f"+ {extraBreaches} extra breach(s)",
                 value="There's more than we can fit in one message."
                 f" [See the rest of them]({hibp['base']})")

            await msg.edit(content="", embed=embed)


def setup(bot):
    bot.add_cog(Security(bot))