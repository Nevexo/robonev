import time
import config
import json
import discord

from discord.ext import commands


class Basic:
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def invite(self, ctx):
        """Sends an invite to add the bot"""
        await ctx.send(f"{ctx.author.mention}: You can use "
                       "<https://discordapp.com/api/oauth2/authorize?"
                       f"client_id={self.bot.user.id}"
                       "&permissions=268435456&scope=bot> "
                       f"to add {self.bot.user.name} to your guild.")

    @commands.command()
    async def hello(self, ctx):
        """Says hello. Duh."""
        await ctx.send(f"Hello {ctx.author.mention}!")

    @commands.command(aliases=['p'])
    async def ping(self, ctx):
        """Shows ping values to discord.

        RTT = Round-trip time, time taken to send a message to discord
        GW = Gateway Ping"""
        before = time.monotonic()
        tmp = await ctx.send('Calculating ping...')
        after = time.monotonic()
        rtt_ms = (after - before) * 1000
        gw_ms = self.bot.latency * 1000

        message_text = f":ping_pong: ROUND TRIP TIME (RTT): `{rtt_ms:.1f}ms`, GATEWAY PING: `{gw_ms:.1f}ms`"
        self.bot.log.info(message_text)
        await tmp.edit(content=message_text)

    @commands.command()
    async def source(self, ctx):
        """ A link to the source code"""
        await ctx.send(f":information_source: You can find the source code at: {config.source}")

    @commands.command(name="credits")
    async def _credits(self, ctx): 
        embed = discord.Embed(title="R. Nev Credits", colour=discord.Colour(0xbbf2c2), description=f"These awesome people helped to build R. Nev, you can contribute aswell [here]({config.source})")

        with open('extra_files/credits.json') as f:
            data = json.load(f)
            print(type(data["credits"]))
            for credit in data["credits"]:
                name = credit["name"]
                value = credit["value"]
                try: 
                    link = credit["link"]
                    value = f"{value} - [Visit Their Website]({link})"
                except:
                    value = value
                embed.add_field(name=name, value=value)
        
        await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(Basic(bot))
