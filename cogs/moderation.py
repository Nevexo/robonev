import config

from discord.ext import commands
from helpers import track

class Moderation:
    def __init__(self, bot):
        self.bot = bot

    def guild_admin(ctx):
        # Checks if the user is a guild administrator
        return ctx.author.guild_permissions.administrator

    def college_server(ctx):
        # Specific for a college guild, not really a shared thing.
        # Sooooo i made it hard config cause i'm lazy as friggers
        if ctx.guild.id == 508932759650172938:
            return True
        else:
            return False

    @commands.check(guild_admin)
    @commands.command()
    async def kick(self, ctx):
        """
            Kicks the mentioned user from your guild"""
        track.exec("Kick Member (r.kick)")
        if len(ctx.message.mentions) > 1 or len(ctx.message.mentions) == 0:
            await ctx.send(":x: Please mention one user.")
        else:
            await ctx.message.mentions[0].kick()
            await ctx.send(f":hammer: Kicked {ctx.message.mentions[0]}")

    @commands.check(guild_admin)
    @commands.command()
    async def ban(self, ctx):
        """
            Bans the mentioned user from your guild"""
        track.exec_cmd("Ban User (r.ban)")
        if len(ctx.message.mentions) > 1 or len(ctx.message.mentions) == 0:
            await ctx.send(":x: Please mention one user.")
        else:
            await ctx.message.mentions[0].ban()
            await ctx.send(f":hammer: Banned {ctx.message.mentions[0]}")

    @commands.check(guild_admin)
    @commands.command()
    async def purge(self, ctx, *, count: int):
        """Purges a number of messages, staff only."""
        track.exec_cmd("Purge channel (r.purge)")
        # Thanks Ave.
        count = 100 if count > 100 else count
        if count <= 0:
            await ctx.send(":x: U insane?")
        else:
            await ctx.channel.purge(limit=count)
            await ctx.send(":white_check_mark: :womans_hat: :ok_hand: Purge complete!")

    @commands.check(guild_admin)
    @commands.check(college_server)
    @commands.command(hidden=True)
    async def naughty(self, ctx):
        """
        Put this geezer in timeout..."""
        NAUGHTY_ROLE_ID = 517480783477342240
        NAUGHTY_ROLE = ctx.guild.get_role(NAUGHTY_ROLE_ID)
        await ctx.message.mentions[0].add_roles(NAUGHTY_ROLE)
        await ctx.message.add_reaction("✅")

def setup(bot):
    bot.add_cog(Moderation(bot))