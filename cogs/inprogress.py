import config
from helpers import track

message = ":information_source: R. Nev is currently undergoing a rewrite in Discord.py[rewrite] - the command you just executed was part of V1 & will be added to V2 within a few days, sorry for the inconvenience ~ Nev."

from discord.ext import commands

class Inprogress:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=["api", "fish", "meme", "stocks", "yt", "marine"], hidden=True)
    async def inprogresscommand(self, ctx):
        track.exec_cmd("An unfinished command.")
        await ctx.send(message)
    
def setup(bot):
    bot.add_cog(Inprogress(bot))