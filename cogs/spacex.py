"""

Robo Nev SpaceX API intergration.

Allows viewing of upcoming launches.

Future Stuffs:
    - Notification on T-1 Hour
    - Notification of scrubbed launch
    - Notification of Landing success/failure

Uses the SpaceXData.com API (version 3) https://api.spacexdata.com/v3/

"""

from discord.ext import commands
import discord
import requests
from helpers import track
class SpaceX():

    def __init__(self, bot):
        self.bot = bot
    
    @commands.command(name="s!next", aliases=["upnext", "nextlaunch"])
    async def _next_launch(self, ctx):

        """
        SpaceX intergration command: See the next SpaceX launch"""
        track.exec_cmd("Next SpaceX Launch (r.s!next)")
        msg = await ctx.send(":clock10: Sending cosmic rays into space...")

        request = requests.get("https://api.spacexdata.com/v3/launches/upcoming?limit=1")

        if request.status_code == 200:
            await msg.edit(content=":clock11: Waiting for a shooting star... ")
            data = request.json()[0]
            embed = discord.Embed(title=f"Next Launch: {data['mission_name']}",
             colour=discord.Colour(0x2578c), # SpaceX Logo colour
             url=data["links"]["reddit_launch"], # Reddit launch information
             description=data["details"] # Details from SpaceX
             )
            note = False
            # Check if the patch has been uploaded

            # Mission patch things (sometimes one doesn't get uploaded to spacexdata)
            if data["links"]["mission_patch"] != None:
                embed.set_thumbnail(url=data["links"]["mission_patch"]) # Mission Patch 
            else:
                note = "Mission patch not yet available."

            embed.set_author(name="SpaceX Launches", url="https://spacex.com",
             icon_url="https://takebackthesky.files.wordpress.com/2017/09/space_x_logo.jpg?w=300&h=300")
            embed.set_footer(text="Data Provided by SpaceXdata.com")

            # Launch Date things
            string = "`" + data["launch_date_utc"] + " (UTC)"
            if data["is_tentative"]:
                string += "(**PROVISIONAL**)"
            string += "`"

            embed.add_field(name="📅 ***LAUNCH DATE***", value=string)

            # Rocket Things
            landingHardware = "No"
            reused = "No"

            core_data = data["rocket"]["first_stage"]["cores"][0]

            if core_data["legs"] and core_data["gridfins"]:
                landingHardware = "Yes"

            if core_data["reused"]:
                reused = f"Yes (flight: {core_data['flight']})"

            embed.add_field(name="🚀 ***ROCKET INFORMATION***",
             value=f"```Name: {data['rocket']['rocket_name']} ({data['rocket']['rocket_type']})"
             f"\nSerial: {data['rocket']['first_stage']['cores'][0]['core_serial']} "
             f"\nLanding H/W: {landingHardware}"
             f"\nReused: {reused}"
             f"\nStatic Fire: {data['static_fire_date_utc']}```",
             inline=False)
            
            # Payload information things
            payload = data["rocket"]["second_stage"]["payloads"][0]
            customers = ""
            for x in payload["customers"]:
                if customers == "":
                    customers = x
                else:
                    customers += f", {x}"
            
            embed.add_field(name="🛰 ***PAYLOAD INFORMATION***",
             value=f"```Name: {payload['payload_id']}"
             f"\nManufacturer: {payload['manufacturer']}"
             f"\nType: {payload['payload_type']}"
             f"\nOrbit: {payload['orbit']}"
             f"\nCustomers: {customers}```",
             inline=False)
            
            # Fairing things

            recoveryAttempt = "***NOT ATTEMPTING RECOVERY***"
            if data["rocket"]["fairings"]["recovery_attempt"] == True:
                recoveryAttempt = "***ATTEMPTING RECOVERY AFTER STAGE SEP.***"
            elif data["rocket"]["fairings"]["recovery_attempt"] == None:
                recoveryAttempt = "***TO BE ANNOUNCED***" 

            embed.add_field(name="🚢 ***FAIRING INFORMATION***",
             value=recoveryAttempt, 
             inline=False)
            
            embed.add_field(name="🔭 ***LAUNCH SITE***",
             value=data["launch_site"]["site_name_long"], 
             inline=False)

            if note != False:
                embed.add_field(name=":information_source: ***NOTICE***",
                value=note,
                inline=False)

            # All done, launch it!
            await msg.edit(content="", embed=embed)
        else:   
            await msg.edit(content=":x: Something went wrong, try again later.")

def setup(bot):
    bot.add_cog(SpaceX(bot))