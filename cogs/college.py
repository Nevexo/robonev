# A module used for keeping track of assignments in our discord server
# Not available for production users, feel free to add your school/college discord into config.py to enable this
# If you want it on the production R. Nev, make a pull requests
# Make data/college.json to use this and initalise it based of the template found [somewhere]
# Commands found in here are hidden to normal users.

from discord.ext import commands
import json
import config

# Helpers

class Helpers:
    DATASTORE_LOCATION = "data/college.json"
    def __init__(self):
        pass

    def read_db(self):
        with open(self.DATASTORE_LOCATION, "r") as read_file:
            data = json.load(read_file)
            return data

    def write_db(self, database):
        with open(self.DATASTORE_LOCATION, "w") as write_file:
            json.dump(database, write_file)
            return True

    def get_assignment(self, database, assignment_id): 
        # NOTE: Kinda crappy code, it's using a linear search.
        # TODO: Refactor this
        found = False
        location = 0
        for assignment in database:
            if assignment['id'] == assignment_id:
                found = location
            location += 1
        return found

class College:
    def __init__(self, bot):
        self.helpers = Helpers()
        self.bot = bot

    def check_is_college(ctx):
        """
            Check if the guild the command was executed in is a guild in guild
            in the list of whitelisted "college/school" servers in
            config.py
        """

        return ctx.guild.id in config.colleges

    @commands.check(check_is_college)
    @commands.command(name='assignment', hidden=True)
    async def test(self, ctx, *, args:str):
        """ Used for the assignments module in the college cog - Hidden to production users """

        args = args.split(" ")
        print(args[0])
        if args[0] == "new":
            """ Create new assignment

                Args:
                    1. Unit number/assignment number (i.e u8/2)
                    2. Tutor First Name
            """
            if len(args) <= 2:
                await ctx.send(":x: Missing arguments, see assignment help page. L1")
            else:
                pending = await ctx.send(":clock10: Pending...")
                # Create assignment object
                assignment = {
                    "id": args[1], #Unit / assignment
                    "tutor": args[2], # Name of Tutor
                    "notes": [{"note_creator": "System", "note": f"Use r.assignment {args[1]} notes add AT 'title'", "note_title": "Assignment Title (AT)"}], # Initalise for later
                    "creator": ctx.author.id # Add the creator (username#discrim afink)
                }

                await pending.edit(content=":clock11: Sync database...")
                # Add assignment to internal database

                # Pull database:
                db = self.helpers.read_db()
                # Add assignment:
                db["assignments"].append(assignment)

                self.helpers.write_db(db)

                await pending.edit(content=f":white_check_mark: Assignment registered. ```json{assignment}```")

        elif args[0] == "delete":
            """
                Delete an assignment
                Args:
                    - Assignment ID (unit/assignment (8/2))
            """

            if len(args) == 2:
                # Pull database
                db_full = self.helpers.read_db()
                db = db_full["assignments"]
                print(args[1])
                assignment_location = self.helpers.get_assignment(db, args[1])
                if assignment_location == False:
                    # Invalid assignment code
                    await ctx.send(f":x: Invalid assignment {args[1]}")
                else:
                    # Assignment code correct, delete it.
                    #self.bot.log(f"Found Assignment {assignment_location} "
                    #"aka {args[2]}")

                    del db[assignment_location]
                    db_full["assignments"] = db                    
                    self.helpers.write_db(db_full)
                    await ctx.send(":white_check_mark: Removed "
                    "assignment object.")

            else:
                ctx.send(":x: Invalid arguments. Check assignment help.")
        elif args[0] == "notes":
            # Stub: Get assignment notes
            return
        elif args[0] == "addnote":
            # Stub: Add new assignment note
            return
        elif args[0] == "help":
            # Stub: Help on the above arguments
            return
        else:
            await ctx.send(":x: Please see assignment help L3")

    @commands.command(name='due', hidden=True)
    async def due(self, ctx):
        await ctx.send("due")

def setup(bot):
    bot.add_cog(College(bot))