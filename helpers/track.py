data_store = {"command_usage": {"pener": 0}, "messages": 0, "oh_counter": 0}

def exec_cmd(cmd):
    if cmd in data_store["command_usage"]:
        data_store["command_usage"][cmd] += 1
    else:
        data_store["command_usage"][cmd] = 1

def fetch_all():
    return data_store

def most_used_cmd():
    top = -1
    top_name = "hmm, i can't decide."
    for cmd in data_store["command_usage"]:
        if data_store["command_usage"][cmd] > top:
            top = data_store["command_usage"][cmd]
            top_name = cmd

    return top_name

def track_messages(msg):
    data_store["messages"] += 1
    if msg == "oh":
        data_store["oh_counter"] += 1

def fetch_messages():
    return data_store["messages"]

def fetch_ohs():
    return data_store["oh_counter"]