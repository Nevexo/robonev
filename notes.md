College assignment tracker

TODO

* r.assignment add unitnumber/assignment (i.e u8/2) requires due date, assignment title.
        - Create role for the assignment and assign it to everyone that does r.assignment opt in (or just by default?)
        - Add assignment to database
        - Ping people weekly until the last 3 days where they are pinged daily (unless they opt out)

* r.assignment finished [assignment number] - removes the role from the user & stop pinging them
* r.due (assignment due) [assignment number] - Gives the remaining time (i.e 5 days, 6 hours, 30 minutes) (possibly update for a few seconds?)
* r.assignment cancel [assignment number] - cancel the specified assignment
* r.assignment edit [assignment number] - remove & recreate assignment
* r.assignment notes [assignment number] - get notes from assignment
* r.assignment addnote [assignment number] [note] - add a note to an assignment
* r.assignment optout - stop being pinged at all by the assignment tracker

### JSON struct

assignments = [{assignment_object}]


#### Assignment object:

{
    "id": "",
    "title": "",
    "tutor": "",
    "notes": [{note_object}],
    "submitted": [{student_object}],
    "creator": {student_object},
    "moodle_link": ""
}

#### Notes Objcet

{
    "note_creator": "",
    "note_title": "THIS.EMPTY",
    "note": "YYYYYYYYYYEEET"
}

#### Student Object

{
    "student_name": "",
    "discord_id": "",
    "cached_name": ""
}

### Update notes:

    - Fix new command structure, title can be multiple words (remove title??)