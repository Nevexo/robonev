# R. Nev (Robonev) rewrite
> R. Nev V 2.0.0

Literally, just a discord bot.

> Prefix: r.

---

Based of [Ao's Discord.Py Rewrite botbase](https://gitlab.com/ao/dpyBotBase)

### Setting up (standalone):

- Install Python 3.6 or higher.
- Install requirements with pip (python -m pip [pip] install -r requirements.txt)
- Configure the ini
- Run python robonev.py

### Setting up (Docker): 

#### Compose

#### Standalone